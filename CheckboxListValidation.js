/*jshint browser: true */
/**
 * Validates the Ridgeway checkbox list control.
 */
( function() {
    function bootstrap() {
        window.ridgeway = window.ridgeway || {};

        window.ridgeway.aspNetValidation = {
            checkBoxValidatorDisableButton: function(chkId, mustBeChecked, btnId) {
                var button = document.getElementById(btnId);
                var chkbox = document.getElementById(chkId);

                if (button && chkbox) {
                    button.disabled = (chkbox.checked != mustBeChecked);
                }
            },

            checkBoxValidatorEvaluateIsValid: function(val) {
                var control = document.getElementById(val.controltovalidate);
                var mustBeChecked = Boolean(val.mustBeChecked == 'true');

                return control.checked == mustBeChecked;
            },

            checkBoxListValidatorEvaluateIsValid: function(val) {
                var control = document.getElementById(val.controltovalidate);
                var minimumNumberOfSelectedCheckBoxes = parseInt(val.minimumNumberOfSelectedCheckBoxes, 10);

                var selectedItemCount = 0;
                var liIndex = 0;
                var currentListItem = document.getElementById(control.id + '_' + liIndex.toString());
                while (currentListItem !== null) {
                    if (currentListItem.checked) selectedItemCount++;
                    liIndex++;
                    currentListItem = document.getElementById(control.id + '_' + liIndex.toString());
                }

                return selectedItemCount >= minimumNumberOfSelectedCheckBoxes;
            }
        };
    }

    // Hook in to AMD
    if( typeof define === 'function' && define.amd ) {
        define( [], bootstrap );
    } else {
        bootstrap();
    }
})();
