/*jshint browser:true, newcap:false */
/*global $, Sys, Page_Validators, Page_ValidationSummaries, ValidatorOnLoad, Page_ValidationActive, ValidatorCommonOnSubmit */
( function() {
    'use strict';

    var dataValidationAttribute = "data-val",
        dataValidationSummaryAttribute = "data-valsummary",
        normalizedAttributes = { validationgroup: "validationGroup", focusonerror: "focusOnError" };

    function getAttributesWithPrefix(element, prefix) {
        var i,
            attribute,
            list = {},
            attributes = element.attributes,
            length = attributes.length,
            prefixLength = prefix.length;
        prefix = prefix.toLowerCase();
        for (i = 0; i < length; i++) {
            attribute = attributes[i];
            if (attribute.specified && attribute.name.substr(0, prefixLength).toLowerCase() === prefix) {
                list[attribute.name.substr(prefixLength)] = attribute.value;
            }
        }

        return list;
    }

    function normalizeKey(key) {
        key = key.toLowerCase();
        return normalizedAttributes[key] === undefined ? key : normalizedAttributes[key];
    }

    function addValidationExpando(element) {
        var attributes = getAttributesWithPrefix(element, dataValidationAttribute + "-");
        $.each(attributes, function (key, value) {
            element[normalizeKey(key)] = value;
        });
    }

    function dispose(element) {
        var index = $.inArray(element, Page_Validators);
        if (index >= 0) {
            Page_Validators.splice(index, 1);
        }
    }

    function addNormalizedAttribute(name, normalizedName) {
        normalizedAttributes[name.toLowerCase()] = normalizedName;
    }

    function parseSpecificAttribute(selector, attribute, validatorsArray) {
        return $(selector).find("[" + attribute + "='true']").each(function (index, element) {
            addValidationExpando(element);
            element.dispose = function () { dispose(element); element.dispose = null; };
            if ($.inArray(element, validatorsArray) === -1) {
                validatorsArray.push(element);
            }
        }).length;
    }

    function parse(selector) {
        var length = parseSpecificAttribute(selector, dataValidationAttribute, Page_Validators);
        length += parseSpecificAttribute(selector, dataValidationSummaryAttribute, Page_ValidationSummaries);
        return length;
    }

    function loadValidators() {
        if (typeof (ValidatorOnLoad) === "function") {
            ValidatorOnLoad();
        }
        if (typeof (ValidatorOnSubmit) === "undefined") {
            window.ValidatorOnSubmit = function () {
                return Page_ValidationActive ? ValidatorCommonOnSubmit() : true;
            };
        }
    }

    function registerUpdatePanel() {
        if (window.Sys && Sys.WebForms && Sys.WebForms.PageRequestManager) {
            var prm = Sys.WebForms.PageRequestManager.getInstance(),
                postBackElement, endRequestHandler;
            if (prm.get_isInAsyncPostBack()) {
                endRequestHandler = function () {
                    if (parse(document)) {
                        loadValidators();
                    }
                    prm.remove_endRequest(endRequestHandler);
                    endRequestHandler = null;
                };
                prm.add_endRequest(endRequestHandler);
            }
            prm.add_beginRequest(function (sender, args) {
                postBackElement = args.get_postBackElement();
            });
            prm.add_pageLoaded(function (sender, args) {
                var i, panels, valFound = 0;
                if (typeof (postBackElement) === "undefined") {
                    return;
                }
                panels = args.get_panelsUpdated();
                for (i = 0; i < panels.length; i++) {
                    valFound += parse(panels[i]);
                }
                panels = args.get_panelsCreated();
                for (i = 0; i < panels.length; i++) {
                    valFound += parse(panels[i]);
                }
                if (valFound) {
                    loadValidators();
                }
            });
        }
    }

    if (typeof (Page_Validators) === "undefined") {
        window.Page_Validators = [];
    }

    if (typeof (Page_ValidationSummaries) === "undefined") {
        window.Page_ValidationSummaries = [];
    }

    if (typeof (Page_ValidationActive) === "undefined") {
        window.Page_ValidationActive = false;
    }

    $.WebFormValidator = {
        addNormalizedAttribute: addNormalizedAttribute,
        parse: parse
    };

    if (parse(document)) {
        window.ridgeway = window.ridgeway || {};
        window.ridgeway.aspNetValidation = window.ridgeway.aspNetValidation || {};

        loadValidators();
    }

    registerUpdatePanel();
})();
