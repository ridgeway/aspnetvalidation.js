/*jshint browser: true */
/*global $, define */
/**
 * Overrides .Net's default validation handlers to give a little more
 * flexibility.
 *
 * TODO: Remove jQuery dependency
 * TODO: Doesn't deal with validation summaries
 * TODO: Nicer scroll to field on error
 *
 * @param {String} opts.form - Selector for the main form element
 * @param {String} opts.wrapper - Selector for the wrapping element around each form field
 * @param {String} opts.invalidClass - Class to be added to invalid elements
 * @param {String} opts.validationElSelector - Selector for the validation elements
 */
( function( $ ) {
    'use strict';

    function AspNetValidation( opts ) {
        if( typeof Page_ClientValidate === 'undefined' ) return;

        var defaults = {
            form: '#aspNetForm',
            wrapper: '.form-field',
            invalidClass: 'is-invalid',
            validationElSelector: '[data-val]'
        };

        this._opts = this._extend( defaults, opts );
        this._methodsToOverride = [ 'ValidatorUpdateDisplay' ];

        this._originalMethods = this._overrideOriginalMethods();
        this._validateAllFields( true, true );
    }

    AspNetValidation.prototype = {
        /**
         * Simple object extension - http://stackoverflow.com/a/12317051
         *
         * @param target {Object} The object in to which source's properties will be merged
         * @param source {Object} The object to be merged in to target
         */
        _extend: function( target, source ) {
            target = target || {};

            for( var prop in source ) {
                if( typeof source[prop] === 'object' ) {
                    target[ prop ] = this._extend( target[ prop ], source[ prop ] );
                } else {
                    target[ prop ] = source[ prop ];
                }
            }

            return target;
        },

        /**
         * Overrides the specified original methods with our new methods
         * whilst storing the old ones for usage later on.
         *
         * @returns {Object}
         */
        _overrideOriginalMethods: function() {
            var $form = $( this._opts.form );

            // We only want to override the methods once
            if( typeof $form.data( 'methodsOverriden' ) !== 'undefined' ) return;

            var that = this;
            var originalMethods = {};
            var applyMethod = function( method ) {
                return function() {
                    that[ method ].apply( that, Array.prototype.slice.call( arguments ) );
                };
            };

            for( var i = 0; i < this._methodsToOverride.length; i++ ) {
                var method = this._methodsToOverride[ i ];

                originalMethods[ method ] = window[ method ];

                window[ method ] = applyMethod( method );
            }

            $form.data( 'methodsOverriden', true );

            return originalMethods;
        },

        /**
         * Triggered when a validation message is found to be valid.
         *
         * @param {Element} msg The message element
         */
        _validMessage: function( msg ) {
            $( msg ).removeClass( this._opts.invalidClass );
        },

        /**
         * Triggered when a validation message is found to be invalid.
         *
         * @param {Element} msg The message element
         * @param {boolean} focusOnError Whether the focusOnError attribute has been set on the back-end
         */
        _invalidMessage: function( msg, focusOnError ) {
            $( msg ).addClass( this._opts.invalidClass );
        },

        /**
         * Checks all messages in the field for their validity.
         *
         * @param {Element} wrapper The field wrapping element
         * @param {boolean} [initialRun=false] Is this the initial run through of validation messages that happens on page load?
         */
        _validateField: function( wrapper, initialRun ) {
            initialRun = typeof initialRun === 'undefined' ? false : initialRun;

            var _this = this;
            var $wrapper = $( wrapper );
            var $messages = $wrapper.find( this._opts.validationElSelector );
            var invalidMessageInField = false;

            if( !initialRun ) $messages.removeClass( 'is-invalid' );

            $wrapper.removeClass( this._opts.invalidClass );

            $messages.each( function() {
                var $msg = $( this );
                var hasInvalidClass = $msg.hasClass( _this._opts.invalidClass );
                var isValid = typeof this.isvalid !== 'undefined' ? this.isvalid : !hasInvalidClass;

                if( isValid ) {
                    _this._validMessage( this );
                } else {
                    _this._invalidMessage( this, initialRun );

                    invalidMessageInField = true;
                }
            });

            if( invalidMessageInField ) {
                $wrapper.addClass( this._opts.invalidClass );
                $wrapper.trigger( 'invalidField' );
            }

            return !invalidMessageInField;
        },

        /**
         * Updates fields that are already invalid in the markup with
         * the overridden functionality.
         *
         * FIXME: This is currently causing all fields to be invalid
         *
         * @param {boolean} focusOnError Whether to focus on the field if it has been specified as a requirement by the back-end
         */
        _validateAllFields: function( focusOnError, initialRun ) {
            var _this = this;

            $( this._opts.validationElSelector ).each( function( i ) {
                _this.ValidatorUpdateDisplay( this, initialRun );
            });
        },

        /**
         * The ValidatorUpdateDisplay override function.
         *
         * @param {Element} msgEl The element that contains all of the meta data about the validation functionality
         * @param {boolean} [initialRun=false] Is this the initial run through of validation messages that happens on page load?
         */
        ValidatorUpdateDisplay: function( msgEl, initialRun ) {
            initialRun = typeof initialRun === 'undefined' ? false : initialRun;

            var $msgEl = $( msgEl );
            var $field = $msgEl.closest( this._opts.wrapper );
            var focusOnError = false;

            this._validateField( $field, initialRun );

            // Remove the default inline styling
            $msgEl.css( 'display', '' );
        },

        /**
         * Public function to check a fields validity.
         *
         * @param {Element} field The field wrapping element
         */
        fieldIsValid: function( field ) {
            return this._validateField( field );
        }
    };

    // Hook in to AMD
    if( typeof define === 'function' && define.amd ) {
        define( [ 'jquery' ], function() {
            return AspNetValidation;
        });
    } else {
        window.AspNetValidation = AspNetValidation;
    }
})( window.jQuery );
