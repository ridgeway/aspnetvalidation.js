# aspNetValidation.js
Hooks in to .Net's validators to add more flexible control for styling.

## Notes
- This doesn't include validation for checkbox lists
- The library currently depends on jQuery so make sure it is present in
your project before using it.

## TODOs
- Check through CheckboxListValidation.js and comment accordingly
- Remove jQuery dependency
- Doesn't deal with validation summaries
- Nicer scroll to field on error

## Changelog

### 27/05/15 (v0.1.3)
- Added ignore list to bower.json

### 27/8/14 (v0.1.2)
- Fixed an issue with references to jQuery's $ variable. 

### 30/6/14 (v0.1.1)
- Cater for messages that aren't ASP validation messages (i.e. a message thats markup is only in the page if it is relevant)

### 23/6/14 (v0.1.0)
- Rewrote to deal with multiple validation messages per field

### 29/10/13
- Added bower.json
- Added CheckboxListValidation.js
- Initial rewrite

### 28/10/13
- Initial commit
